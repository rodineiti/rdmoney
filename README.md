## 🚀 RD Money

<br>

## 🧪 Tecnologias

Esse projeto foi desenvolvido com as seguintes tecnologias:

- [React](https://reactjs.org)
- [MirageJs](https://miragejs.com/)
- [TypeScript](https://www.typescriptlang.org/)

## 🚀 Como executar

Clone o projeto e acesse a pasta do mesmo.

```bash
$ git clone git@gitlab.com:rodineiti/rdmoney.git
$ cd rdmoney
```

Para iniciá-lo, siga os passos abaixo:

```bash
# Instalar as dependências
$ yarn

# Iniciar o projeto
$ yarn start
```

O app estará disponível no seu browser pelo endereço http://localhost:3000.

## 💻 Screen

![Animação](/uploads/eebc9ab2ef16d6d8a2482e4d84d6a4e1/Animação.gif)

## 💻 Demo

https://rdmoney.vercel.app/

## 📝 License

Esse projeto está sob a licença MIT.
