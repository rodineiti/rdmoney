import axios from "axios";

const api = axios.create({
  baseURL: "https://rdmoney.vercel.app/api",
});

api.interceptors.request.use(
  function (config) {
    addLoading();
    return config;
  },
  function (err) {
    return Promise.reject(err);
  }
);

api.interceptors.response.use(
  function (response) {
    removeLoading();
    return response;
  },
  function (err) {
    removeLoading();
    return Promise.reject(err);
  }
);

const addLoading = () => {
  document.body.classList.add("preloader");
  const sidebar = document.getElementById("sidebar");
  if (sidebar) {
    sidebar.classList.remove("active");
  }

  if (!document.getElementById("preloader")) {
    const divPreloader = document.createElement("div");
    divPreloader.setAttribute("id", "preloader");

    const divBox = document.createElement("div");
    divBox.classList.add("preloader-box");

    for (let i = 0; i <= 3; i++) {
      const div = document.createElement("div");
      div.classList.add("item");
      div.classList.add("item-" + (i + 1));
      divBox.appendChild(div);
    }

    divPreloader.appendChild(divBox);

    document.body.appendChild(divPreloader);
  }
};

const removeLoading = () => {
  document.body.classList.remove("preloader");
  const preloader = document.getElementById("preloader");
  if (preloader) {
    preloader.remove();
  }
};

export { api };
