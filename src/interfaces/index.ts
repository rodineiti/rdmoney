import { ReactNode } from "react";

export interface Transaction {
  id: number;
  title: string;
  amount: number;
  category: string;
  createdAt: string;
  type: string;
}

export type TransactionForm = Omit<Transaction, "id" | "createdAt">;

export interface TransactionsProviderProps {
  children: ReactNode;
}

export interface TransactionsContextData {
  transaction: Transaction;
  transactions: Transaction[];
  createTransaction: (transaction: TransactionForm) => Promise<void>;
  updateTransaction: (transaction: Transaction) => Promise<void>;
  showTransaction: (id: number) => Promise<void>;
  deleteTransaction: (id: number) => Promise<void>;
}

export interface HeaderProps {
  onOpenNewTransactionModal: () => void;
  onOpenDeleteTransactionModal?: () => void;
}

export interface NewTransactionModalProps {
  isOpen: boolean;
  onRequestClose: () => void;
}

export interface RadioButtonProps {
  isActive: boolean;
  activeColor: "green" | "red";
}

export interface InputProps {
  hasError: boolean;
}
