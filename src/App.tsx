import { useState } from "react";
import { ToastContainer } from "react-toastify";
import { Header } from "./components/Header";
import { Dashboard } from "./components/Dashboard";
import { NewTransactionModal } from "./components/NewTransactionModal";
import { EditTransactionModal } from "./components/EditTransactionModal";
import { DeleteTransactionModal } from "./components/DeleteTransactionModal";
import { TransactionsProvider } from "./contexts/TransactionsContext";
import "react-toastify/dist/ReactToastify.css";
import { GlobalStyle } from "./styles/global";

export function App() {
  const [isOpenModalTransaction, setIsOpenModalTransaction] = useState(false);
  const [isOpenModalTransactionEdit, setIsOpenModalTransactionEdit] =
    useState(false);
  const [isOpenModalTransactionDelete, setIsOpenModalTransactionDelete] =
    useState(false);

  function handleOpenNewTransactionModal() {
    setIsOpenModalTransaction(true);
  }

  function handleCloseNewTransactionModal() {
    setIsOpenModalTransaction(false);
  }

  function handleOpenEditTransactionModal() {
    setIsOpenModalTransactionEdit(true);
  }

  function handleCloseEditTransactionModal() {
    setIsOpenModalTransactionEdit(false);
  }

  function handleOpenDeleteTransactionModal() {
    setIsOpenModalTransactionDelete(true);
  }

  function handleCloseDeleteTransactionModal() {
    setIsOpenModalTransactionDelete(false);
  }

  return (
    <TransactionsProvider>
      <Header onOpenNewTransactionModal={handleOpenNewTransactionModal} />

      <Dashboard
        onOpenNewTransactionModal={handleOpenEditTransactionModal}
        onOpenDeleteTransactionModal={handleOpenDeleteTransactionModal}
      />

      {isOpenModalTransaction && (
        <NewTransactionModal
          isOpen={isOpenModalTransaction}
          onRequestClose={handleCloseNewTransactionModal}
        />
      )}

      {isOpenModalTransactionEdit && (
        <EditTransactionModal
          isOpen={isOpenModalTransactionEdit}
          onRequestClose={handleCloseEditTransactionModal}
        />
      )}

      {isOpenModalTransactionDelete && (
        <DeleteTransactionModal
          isOpen={isOpenModalTransactionDelete}
          onRequestClose={handleCloseDeleteTransactionModal}
        />
      )}

      <GlobalStyle />
      <ToastContainer />
    </TransactionsProvider>
  );
}
