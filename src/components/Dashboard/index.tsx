import styled from "styled-components";
import { HeaderProps } from "../../interfaces";
import { Summary } from "../Summary";
import { TransactionTable } from "../TransactionTable";

const Container = styled.div`
  max-width: 1120px;
  margin: 0 auto;
  padding: 2.5rem 1rem;
`;

export function Dashboard({
  onOpenNewTransactionModal,
  onOpenDeleteTransactionModal,
}: HeaderProps) {
  return (
    <Container>
      <Summary />
      <TransactionTable
        onOpenNewTransactionModal={onOpenNewTransactionModal}
        onOpenDeleteTransactionModal={onOpenDeleteTransactionModal}
      />
    </Container>
  );
}
