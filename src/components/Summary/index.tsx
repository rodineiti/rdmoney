import styled from "styled-components";
import { useTransactions } from "../../hooks/useTransactions";
import incomeSvg from "./../../assets/income.svg";
import outcomeSvg from "./../../assets/outcome.svg";
import totalSvg from "./../../assets/total.svg";

const Container = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr); // alternative: 1fr 1fr 1fr
  gap: 2rem;
  margin-top: -10rem;

  @media (max-width: 820px) {
    display: flex;
    flex-direction: column;
  }

  div {
    background: var(--shape);
    padding: 1.5rem 2rem;
    border-radius: 0.25rem;
    color: var(--text-title);

    header {
      display: flex;
      align-items: center;
      justify-content: space-between;
    }

    strong {
      display: block;
      margin-top: 1rem;
      font-size: 2rem;
      font-weight: 500;
      line-height: 3rem;
    }

    &.highlight-background {
      background: var(--blue);
      color: var(--shape);
    }
  }
`;

export function Summary() {
  const { transactions } = useTransactions();

  const summary = transactions.reduce(
    (acomulator, transaction) => {
      if (transaction.type === "deposit") {
        acomulator.deposits += transaction.amount;
        acomulator.total += transaction.amount;
      } else {
        acomulator.withdraw += transaction.amount;
        acomulator.total -= transaction.amount;
      }

      return acomulator;
    },
    {
      deposits: 0,
      withdraw: 0,
      total: 0,
    }
  );

  return (
    <Container>
      <div>
        <header>
          <p>Incomes</p>
          <img src={incomeSvg} alt="Incomes" />
        </header>
        <strong>
          +{" "}
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
          }).format(summary.deposits)}
        </strong>
      </div>
      <div>
        <header>
          <p>Outcomes</p>
          <img src={outcomeSvg} alt="Outcomes" />
        </header>
        <strong>
          -{" "}
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
          }).format(summary.withdraw)}
        </strong>
      </div>
      <div className="highlight-background">
        <header>
          <p>Total</p>
          <img src={totalSvg} alt="Total" />
        </header>
        <strong>
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
          }).format(summary.total)}
        </strong>
      </div>
    </Container>
  );
}
