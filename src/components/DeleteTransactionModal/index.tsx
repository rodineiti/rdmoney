import { FormEvent } from "react";
import Modal from "react-modal";
import { toast } from "react-toastify";
import styled from "styled-components";
import { useTransactions } from "../../hooks/useTransactions";
import { NewTransactionModalProps } from "../../interfaces";
import closeSvg from "./../../assets/close.svg";

const Container = styled.form`
  h2 {
    color: var(--text-title);
    font-size: 1.5rem;
    margin-bottom: 2rem;
  }

  p {
    text-align: center;
    font-weight: 500;
  }

  button[type="submit"] {
    width: 100%;
    padding: 0 1.5rem;
    height: 4rem;
    background: var(--red);
    color: #fff;
    border-radius: 0.25rem;
    border: 0;
    font-size: 1rem;
    font-weight: 600;
    margin-top: 1.5rem;
    transition: filter 0.2s;

    &:hover {
      filter: brightness(0.9);
    }
  }
`;

Modal.setAppElement("#root");

export function DeleteTransactionModal({
  isOpen,
  onRequestClose,
}: NewTransactionModalProps) {
  const { transaction, deleteTransaction } = useTransactions();

  async function handleDeleteTransaction(event: FormEvent) {
    event.preventDefault();

    await deleteTransaction(transaction.id);

    toast.success("Deleted successfully");

    onRequestClose();
  }

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      overlayClassName="react-modal-overlay"
      className="react-modal-content"
    >
      <Container onSubmit={handleDeleteTransaction}>
        <button
          className="react-modal-close"
          type="button"
          onClick={onRequestClose}
        >
          <img src={closeSvg} alt="close" />
        </button>

        <h2>Delete Transaction</h2>
        <p>Confirm delete?</p>
        <button type="submit">Delete</button>
      </Container>
    </Modal>
  );
}
