import styled from "styled-components";
import { HeaderProps } from "../../interfaces";

import logoSvg from "./../../assets/logo.svg";
import logoHeaderSvg from "./../../assets/logo-header.png";

const Container = styled.header`
  background: var(--green);
`;

const Content = styled.div`
  max-width: 1120px;
  margin: 0 auto;
  padding: 2rem 1rem 12rem;
  display: flex;
  align-items: center;
  justify-content: space-between;

  .logo-header {
    display: flex;
    justify-content: center;
    align-items: center;

    .logo {
      margin-left: -8.5rem;
    }
  }

  @media (max-width: 720px) {
    .logo-header {
      .logo {
        margin-left: -9.5rem;
      }
    }
  }

  @media (max-width: 520px) {
    flex-direction: column;

    .logo-header {
      .logo {
        margin-left: -9.5rem;
      }
    }
  }

  button {
    font-size: 1rem;
    color: var(--shape);
    background: var(--blue-light);
    border: 0;
    padding: 0 2rem;
    border-radius: 0.25rem;
    height: 3rem;

    transition: filter 0.2s;

    &:hover {
      filter: brightness(0.9);
    }

    @media (max-width: 520px) {
      margin-top: 1rem;
    }
  }
`;

export function Header({ onOpenNewTransactionModal }: HeaderProps) {
  return (
    <Container>
      <Content>
        <div className="logo-header">
          <img src={logoSvg} alt="rd money" />
          <img src={logoHeaderSvg} alt="rd money" className="logo" />
        </div>
        <button type="button" onClick={onOpenNewTransactionModal}>
          New Transaction
        </button>
      </Content>
    </Container>
  );
}
