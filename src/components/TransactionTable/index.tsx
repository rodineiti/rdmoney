import styled from "styled-components";
import { useTransactions } from "../../hooks/useTransactions";
import { HeaderProps } from "../../interfaces";

import deleteSvg from "./../../assets/delete.svg";
import editSvg from "./../../assets/edit.svg";

const Container = styled.div`
  margin-top: 4rem;

  @media (max-width: 820px) {
    overflow-x: auto;
  }

  table {
    width: 100%;
    border-spacing: 0 0.5rem;

    th {
      color: var(--text-body);
      font-weight: 400;
      padding: 1rem 2rem;
      text-align: right;
      line-height: 1.5rem;
    }

    td {
      padding: 1rem 2rem;
      border: 0;
      background: var(--shape);
      color: var(--text-body);
      border-radius: 0.25rem;

      &:first-child {
        color: var(--text-title);
      }

      &.deposit {
        color: var(--green);
      }

      &.withdraw {
        color: var(--red);
      }

      &.options {
        button {
          border: 0;
          background: transparent;

          img {
            width: 20px;
            height: 20px;
            margin-right: 1rem;
          }
        }
      }
    }
  }
`;

export function TransactionTable({
  onOpenNewTransactionModal,
  onOpenDeleteTransactionModal,
}: HeaderProps) {
  const { transactions, showTransaction } = useTransactions();

  async function getTransaction(id: number) {
    await showTransaction(id);
    onOpenNewTransactionModal();
  }

  async function getTransactionDelete(id: number) {
    await showTransaction(id);
    onOpenDeleteTransactionModal && onOpenDeleteTransactionModal();
  }

  return (
    <Container>
      <table>
        <thead>
          <tr>
            <th>Title</th>
            <th>Amount</th>
            <th>Category</th>
            <th>Created At</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          {transactions.map((item, index) => (
            <tr key={`item_${String(index)}`}>
              <td>{item.title}</td>
              <td className={item.type}>
                {new Intl.NumberFormat("en-US", {
                  style: "currency",
                  currency: "USD",
                }).format(item.amount)}
              </td>
              <td>{item.category}</td>
              <td>
                {new Intl.DateTimeFormat("en-US").format(
                  new Date(item.createdAt)
                )}
              </td>
              <td className="options">
                <button type="button" onClick={() => getTransaction(item.id)}>
                  <img src={editSvg} alt="edit" />
                </button>
                <button
                  type="button"
                  onClick={() => getTransactionDelete(item.id)}
                >
                  <img src={deleteSvg} alt="delete" />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </Container>
  );
}
