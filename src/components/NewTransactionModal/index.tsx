import { FormEvent, useState } from "react";
import Modal from "react-modal";
import { toast } from "react-toastify";
import styled from "styled-components";
import { darken, transparentize } from "polished";
import { useTransactions } from "../../hooks/useTransactions";
import {
  NewTransactionModalProps,
  RadioButtonProps,
  InputProps,
} from "../../interfaces";
import closeSvg from "./../../assets/close.svg";
import incomeSvg from "./../../assets/income.svg";
import outcomeSvg from "./../../assets/outcome.svg";

const colors = {
  green: "#18bc9c",
  red: "#e52e4d",
};

const Container = styled.form`
  h2 {
    color: var(--text-title);
    font-size: 1.5rem;
    margin-bottom: 2rem;
  }

  span {
    font-size: 0.75rem;
    color: var(--red);
  }

  button[type="submit"] {
    width: 100%;
    padding: 0 1.5rem;
    height: 4rem;
    background: var(--blue);
    color: #fff;
    border-radius: 0.25rem;
    border: 0;
    font-size: 1rem;
    font-weight: 600;
    margin-top: 1.5rem;
    transition: filter 0.2s;

    &:hover {
      filter: brightness(0.9);
    }
  }
`;

const Input = styled.input<InputProps>`
  width: 100%;
  min-width: 200px;
  padding: 0 1.5rem;
  height: 4rem;
  border-radius: 0.25rem;
  border: 1px solid ${({ hasError }) => (hasError ? "red" : "#d7d7d7")};
  background: #e7e9ee;
  font-weight: 400;
  font-size: 1rem;

  &::placeholder {
    color: var(--text-body);
  }

  & + input {
    margin-top: 1rem;
  }
`;

const TypesContainer = styled.div`
  margin: 1rem 0;
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 0.5rem;
`;

const RadioButton = styled.button<RadioButtonProps>`
  height: 4rem;
  border-radius: 0.25rem;
  border: 1px solid #d7d7d7;
  background: ${({ isActive, activeColor }) =>
    isActive ? transparentize(0.9, colors[activeColor]) : "transparent"};
  display: flex;
  align-items: center;
  justify-content: center;
  transition: border-color 0.2s;

  &:hover {
    border-color: ${darken(0.1, "#d7d7d7")};
  }

  img {
    width: 20px;
    height: 20px;
  }

  span {
    display: inline-block;
    margin-left: 1rem;
    font-size: 1rem;
    color: var(--text-title);
  }
`;

Modal.setAppElement("#root");

export function NewTransactionModal({
  isOpen,
  onRequestClose,
}: NewTransactionModalProps) {
  const { createTransaction } = useTransactions();
  const [type, setType] = useState("deposit");
  const [title, setTitle] = useState("");
  const [amount, setAmount] = useState("");
  const [category, setCategory] = useState("");
  const [errors, setErrors] = useState<string[]>([]);

  function handleSetType(value: string) {
    setType(value);
  }

  async function handleCreateNewTransaction(event: FormEvent) {
    event.preventDefault();
    let currentErrors = [...errors];

    if (!title) {
      currentErrors.push("title");
      setErrors(currentErrors);
      toast.error("Please, enter the title");
      return;
    }

    if (!amount) {
      currentErrors.push("amount");
      setErrors(currentErrors);
      toast.error("Please, enter the amount");
      return;
    }

    if (!category) {
      currentErrors.push("category");
      setErrors(currentErrors);
      toast.error("Please, enter the category");
      return;
    }

    await createTransaction({
      title,
      amount: Number(amount),
      category,
      type,
    });

    toast.success("Saved successfully");

    clearData();
  }

  function clearData() {
    setType("deposit");
    setTitle("");
    setAmount("");
    setCategory("");
    onRequestClose();
  }

  function clearError(value: string) {
    let newErrors = [...errors];
    let index = newErrors.indexOf(value);
    if (index !== -1) {
      newErrors.splice(index, 1);
      setErrors(newErrors);
    }
  }

  return (
    <Modal
      isOpen={isOpen}
      closeTimeoutMS={500}
      onRequestClose={onRequestClose}
      overlayClassName="react-modal-overlay"
      className="react-modal-content"
    >
      <Container onSubmit={handleCreateNewTransaction}>
        <button
          className="react-modal-close"
          type="button"
          onClick={onRequestClose}
        >
          <img src={closeSvg} alt="close" />
        </button>

        <h2>Create Transaction</h2>

        <Input
          hasError={errors.includes("title")}
          type="text"
          placeholder="Title"
          value={title}
          onChange={(event) => {
            setTitle(event.target.value);
            clearError("title");
          }}
        />
        {errors.includes("title") && <span>Please, enter the title</span>}

        <Input
          hasError={errors.includes("amount")}
          type="number"
          placeholder="Amount"
          value={amount}
          onChange={(event) => {
            setAmount(event.target.value);
            clearError("amount");
          }}
        />
        {errors.includes("amount") && <span>Please, enter the amount</span>}

        <TypesContainer>
          <RadioButton
            type="button"
            onClick={() => handleSetType("deposit")}
            isActive={type === "deposit"}
            activeColor="green"
          >
            <img src={incomeSvg} alt="income" />
            <span>Income</span>
          </RadioButton>
          <RadioButton
            type="button"
            onClick={() => handleSetType("withdraw")}
            isActive={type === "withdraw"}
            activeColor="red"
          >
            <img src={outcomeSvg} alt="outcome" />
            <span>Outcome</span>
          </RadioButton>
        </TypesContainer>

        <Input
          hasError={errors.includes("category")}
          type="text"
          placeholder="Category"
          value={category}
          onChange={(event) => {
            setCategory(event.target.value);
            clearError("category");
          }}
        />
        {errors.includes("category") && <span>Please, enter the category</span>}

        <button type="submit">Save</button>
      </Container>
    </Modal>
  );
}
