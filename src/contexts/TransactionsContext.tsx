import { createContext, useEffect, useState } from "react";
import { api } from "../services/api";

import {
  Transaction,
  TransactionForm,
  TransactionsContextData,
  TransactionsProviderProps,
} from "./../interfaces";

export const TransactionsContext = createContext<TransactionsContextData>(
  {} as TransactionsContextData
);

export function TransactionsProvider({ children }: TransactionsProviderProps) {
  const [transaction, setTransaction] = useState<Transaction>(
    {} as Transaction
  );
  const [transactions, setTransactions] = useState<Transaction[]>([]);

  useEffect(() => {
    (async () => {
      const { data } = await api.get("/transactions");
      setTransactions(data.transactions);
    })();
  }, []);

  async function createTransaction(formData: TransactionForm) {
    const response = await api.post("/transactions", {
      ...formData,
      createdAt: new Date(),
    });
    setTransactions([...transactions, response.data.transaction]);
  }

  async function showTransaction(id: number) {
    const response = await api.get(`/transactions/${id}`);
    setTransaction(response.data);
  }

  async function updateTransaction(formData: Transaction) {
    const response = await api.put(`/transactions/${formData.id}`, formData);
    let newTransactions = transactions.map((item) =>
      item.id === formData.id ? response.data : item
    );
    setTransactions(newTransactions);
  }

  async function deleteTransaction(id: number) {
    await api.delete(`/transactions/${id}`);
    let newTransactions = transactions.filter((item) => item.id !== id);
    setTransactions(newTransactions);
  }

  return (
    <TransactionsContext.Provider
      value={{
        transaction,
        transactions,
        createTransaction,
        showTransaction,
        updateTransaction,
        deleteTransaction,
      }}
    >
      {children}
    </TransactionsContext.Provider>
  );
}
