import React from "react";
import ReactDOM from "react-dom";
import { createServer, Model } from "miragejs";
import { App } from "./App";

createServer({
  models: {
    transaction: Model,
  },

  seeds(server) {
    server.db.loadData({
      transactions: [
        {
          id: 1,
          title: "Transaction 1",
          type: "deposit",
          category: "Developer",
          amount: 1000,
          createdAt: new Date("2021-07-28 19:00:00"),
        },
        {
          id: 2,
          title: "Transaction 2",
          type: "withdraw",
          category: "Home",
          amount: 1000,
          createdAt: new Date("2021-07-25 19:00:00"),
        },
      ],
    });
  },

  routes() {
    this.namespace = "api";

    this.get("/transactions", () => {
      return this.schema.all("transaction");
    });

    this.post("/transactions", (schema, request) => {
      const body = JSON.parse(request.requestBody);
      return schema.create("transaction", body);
    });

    this.get("/transactions/:id", (schema, request) => {
      const id = request.params.id;
      return schema.db.transactions.find(id);
    });

    this.put("/transactions/:id", (schema, request) => {
      const body = JSON.parse(request.requestBody);
      return schema.db.transactions.update(body.id, body);
    });

    this.delete("/transactions/:id", (schema: any, request) => {
      return schema.db.transactions.remove(request.params.id);
    });
  },
});

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
