import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  :root {
    --background: #f0f2f5;
    --red: #e52e4d;
    --green: #18bc9c;
    --blue: #009FFD;
    --blue-light: #25a7c2;
    --text-title: #363f5f;
    --text-body: #969cb3;
    --shape: #ffffff;
  }

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  // font-size: 16px (Desktop)
  html {
    @media (max-width: 1080px) {
      font-size: 93.75%; // 15px
    }

    @media (max-width: 720px) {
      font-size: 87.5%; // 14px
    }
  }

  body {
    background: var(--background);
    -webkit-font-smoothing: antialiased;
  }

  body, input, textarea, button {
    font-family: 'Poppins', sans-serif;
    font-weight: 400;
  }

  h1, h2, h3, h4, h5, h6, strong {
    font-weight: 600;
  }

  button {
    cursor: pointer;
  }

  [disabled] {
    opacity: 0.6;
    cursor: not-allowed;
  }

  /** Modal Start */
  .react-modal-overlay {
    background: rgba(0,0,0,0.5);
    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    opacity: 0;
    transform: translateX(-100px);
    transition: all 500ms ease-in-out;
  }

  .ReactModal__Overlay--after-open {
    opacity: 1;
    transform: translateX(0px);
  }

  .ReactModal__Overlay--before-close {
    opacity: 0;
    transform: translateX(-100px);
  }

  .react-modal-content {
    width: 100%;
    max-width: 576px;
    background: var(--background);
    padding: 3rem;
    position: relative;
    border-radius: 0%.24rem;

    @media (max-width: 720px) {
      width: 90%;
    }
  }

  .react-modal-close {
    position: absolute;
    right: 1.5rem;
    top: 1.5rem;
    border: 0;
    background: transparent;
    transition: filter 0.2s;

    &:hover {
      filter: brightness(0.8);
    }
  }
  /** Modal End */

  /** Preloader Start */
  #preloader {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(255, 255, 255, 0.7);
    z-index: 999;

    .preloader-box {
      position: absolute;
      top: 50%;
      left: 50%;
      width: 100px;
      height: 100px;
      transform: translate(-50%, -50%);
      padding: 15px 30px;
      overflow: hidden;

      .item {
        width: 50px;
        height: 50px;
        position: absolute;
      }

      .item-1 {
        background-color: #fa5667;
        top: 0;
        left: 0;
        z-index: 1;
        animation: item-1_move 1.8s cubic-bezier(0.6, 0.01, 0.4, 1) infinite;
      }
      .item-2 {
        background-color: #7a45e5;
        top: 0;
        right: 0;
        animation: item-2_move 1.8s cubic-bezier(0.6, 0.01, 0.4, 1) infinite;
      }
      .item-3 {
        background-color: #1b91f7;
        bottom: 0;
        right: 0;
        animation: item-3_move 1.8s cubic-bezier(0.6, 0.01, 0.4, 1) infinite;
      }
      .item-4 {
        background-color: #fac24c;
        bottom: 0;
        left: 0;
        animation: item-4_move 1.8s cubic-bezier(0.6, 0.01, 0.4, 1) infinite;
      }
    }

    @keyframes item-1_move {
      0%,
      100% {
        transform: translate(0, 0);
      }
      25% {
        transform: translate(0, 50px);
      }
      50% {
        transform: translate(50px, 50px);
      }
      75% {
        transform: translate(50px, 0);
      }
    }
    @keyframes item-2_move {
      0%,
      100% {
        transform: translate(0, 0);
      }
      25% {
        transform: translate(-50px, 0);
      }
      50% {
        transform: translate(-50px, 50px);
      }
      75% {
        transform: translate(0, 50px);
      }
    }
    @keyframes item-3_move {
      0%,
      100% {
        transform: translate(0, 0);
      }
      25% {
        transform: translate(0, -50px);
      }
      50% {
        transform: translate(-50px, -50px);
      }
      75% {
        transform: translate(-50px, 0);
      }
    }
    @keyframes item-4_move {
      0%,
      100% {
        transform: translate(0, 0);
      }
      25% {
        transform: translate(50px, 0);
      }
      50% {
        transform: translate(50px, -50px);
      }
      75% {
        transform: translate(0, -50px);
      }
    }
  } 

  /** Preloader End */
`;
